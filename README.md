# PhoneBookSwing
Phone Book Windows Client with Swing

This is a Windows Client application. To use it you need to have access to a running version of the backend services on a computer. After that you can give the IP address of the computer that is running the server side to this application and start using it.
Note that you CAN run the server locally. In that case, just enter `localhost` in this app or enter `127.0.0.1`.
