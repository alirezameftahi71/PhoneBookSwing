package test;

import java.io.IOException;

import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;

import main.java.controller.PersonManager;
import main.java.controller.util.Connector;
import main.java.controller.util.Handler;
import main.java.model.Person;

public class personManagerTest {
	private Person person;

	@Before
	public void setup() {
		Connector.setIp("127.0.0.1");
		person = new Person("user", Handler.getMD5Hash("pass"));

	}

	@Test
	public void login() throws IOException, RuntimeException {
		int statusCode = PersonManager.login(person);
		Assert.assertEquals(200, statusCode);
	}
}
