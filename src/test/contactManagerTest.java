package test;

import java.util.ArrayList;
import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;
import main.java.controller.ContactManager;
import main.java.controller.util.Connector;
import main.java.model.Contact;

public class contactManagerTest {
	private Contact contact;

	@Before
	public void setup() {
		Connector.setIp("127.0.0.1");
		contact = new Contact("Alireza", "Meftahi", "0263460", "09393744769", "alirezameftahi_71@yahoo.com");
	}

	@Test
	public void add() throws Exception {
		ContactManager.add(contact);
		ArrayList<Contact> contacts = (ArrayList<Contact>) ContactManager.getAll();
		Assert.assertEquals(contact.getFirstName(), contacts.get(contacts.size() - 1).getFirstName());
	}

	@Test
	public void getAll() throws Exception {
		int sizeBefore = ContactManager.getAll().size();
		ContactManager.add(contact);
		int sizeAfter = ContactManager.getAll().size();
		Assert.assertEquals(sizeBefore + 1, sizeAfter);
	}
}
