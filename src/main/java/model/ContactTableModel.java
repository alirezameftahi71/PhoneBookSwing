package main.java.model;

import java.util.ArrayList;
import java.util.List;
import javax.swing.table.AbstractTableModel;

public class ContactTableModel extends AbstractTableModel {

	private static final long serialVersionUID = 1L;
	private List<Contact> contacts;

	public ContactTableModel(List<Contact> contacts) {
		this.contacts = new ArrayList<>(contacts);
	}

	@Override
	public int getRowCount() {
		return contacts.size();
	}

	@Override
	public int getColumnCount() {
		return 5;
	}

	@Override
	public String getColumnName(int column) {
		String name = "";
		switch (column) {
		case 0:
			name = "First Name";
			break;
		case 1:
			name = "Last Name";
			break;
		case 2:
			name = "Home Tel";
			break;
		case 3:
			name = "Mobile";
			break;
		case 4:
			name = "Email";
			break;
		}
		return name;
	}

	@Override
	public Object getValueAt(int rowIndex, int columnIndex) {
		Contact contact = contacts.get(rowIndex);
		Object value = null;
		switch (columnIndex) {
		case 0:
			value = contact.getFirstName();
			break;
		case 1:
			value = contact.getLastName();
			break;
		case 2:
			value = contact.getHomeTel();
			break;
		case 3:
			value = contact.getMobile();
			break;
		case 4:
			value = contact.getEmail();
			break;
		}
		return value;
	}
}