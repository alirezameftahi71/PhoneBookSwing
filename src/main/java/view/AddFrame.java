package main.java.view;

import java.awt.EventQueue;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.io.IOException;

import javax.swing.JButton;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.JTextField;

import main.java.controller.ContactManager;
import main.java.controller.util.PopupBox;
import main.java.model.Contact;

public class AddFrame {
	private JFrame addFrame;
	private JTextField tfFirstName;
	private JTextField tfLastName;
	private JTextField tfHomeTel;
	private JTextField tfMobile;
	private JTextField tfEmail;

	/**
	 * Launch the application.
	 */
	public void show() {
		EventQueue.invokeLater(new Runnable() {
			@Override
			public void run() {
				try {
					AddFrame window = new AddFrame();
					window.addFrame.setVisible(true);
				} catch (Exception e) {
					e.printStackTrace();
				}
			}
		});
	}

	/**
	 * Create the application.
	 */
	public AddFrame() {
		initialize();
	}

	public void reset() {
		tfFirstName.setText(null);
		tfLastName.setText(null);
		tfHomeTel.setText(null);
		tfMobile.setText(null);
		tfEmail.setText(null);
	}

	/**
	 * Initialize the contents of the frame.
	 */
	private void initialize() {
		addFrame = new JFrame();
		addFrame.setResizable(false);
		addFrame.setTitle("Add Contact");
		addFrame.setBounds(100, 100, 686, 282);
		addFrame.setDefaultCloseOperation(JFrame.DISPOSE_ON_CLOSE);
		addFrame.getContentPane().setLayout(null);

		JPanel panelContactSave = new JPanel();
		panelContactSave.setBounds(10, 11, 659, 230);
		addFrame.getContentPane().add(panelContactSave);
		panelContactSave.setLayout(null);

		JLabel lblFirstName = new JLabel("First Name");
		lblFirstName.setBounds(10, 35, 81, 30);
		panelContactSave.add(lblFirstName);

		tfFirstName = new JTextField();
		tfFirstName.setBounds(101, 35, 159, 30);
		panelContactSave.add(tfFirstName);
		tfFirstName.setColumns(10);

		tfLastName = new JTextField();
		tfLastName.setBounds(101, 98, 159, 30);
		panelContactSave.add(tfLastName);
		tfLastName.setColumns(10);

		tfHomeTel = new JTextField();
		tfHomeTel.setBounds(435, 35, 159, 30);
		panelContactSave.add(tfHomeTel);
		tfHomeTel.setColumns(10);

		tfMobile = new JTextField();
		tfMobile.setBounds(435, 98, 159, 30);
		panelContactSave.add(tfMobile);
		tfMobile.setColumns(10);

		JLabel lblLastName = new JLabel("Last Name");
		lblLastName.setBounds(10, 98, 81, 30);
		panelContactSave.add(lblLastName);

		JLabel lblHomeTel = new JLabel("Home Tel");
		lblHomeTel.setBounds(344, 35, 81, 30);
		panelContactSave.add(lblHomeTel);

		JLabel lblMobile = new JLabel("Mobile");
		lblMobile.setBounds(344, 98, 81, 30);
		panelContactSave.add(lblMobile);

		tfEmail = new JTextField();
		tfEmail.setColumns(10);
		tfEmail.setBounds(435, 161, 159, 30);
		panelContactSave.add(tfEmail);

		JLabel lblEmail = new JLabel("Email");
		lblEmail.setBounds(344, 161, 81, 30);
		panelContactSave.add(lblEmail);

		JButton btnSaveContact = new JButton("Save");
		btnSaveContact.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent arg0) {
				try {
					Contact contact = new Contact(tfFirstName.getText(), tfLastName.getText(), tfHomeTel.getText(),
							tfMobile.getText(), tfEmail.getText());
					int statusCode = ContactManager.add(contact);
					if (statusCode == 200) {
						PopupBox.showMessage("Contact Added Successfully!", "Seccess!");
						reset();
					}
				} catch (RuntimeException | IOException e) {
					PopupBox.showError(e.getMessage(), "Error");
				}
			}
		});
		btnSaveContact.setBounds(10, 161, 103, 30);
		panelContactSave.add(btnSaveContact);

		JButton btnClear = new JButton("Cancel");
		btnClear.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent arg0) {
				addFrame.dispose();
			}
		});
		btnClear.setBounds(157, 161, 103, 30);
		panelContactSave.add(btnClear);
	}
}
