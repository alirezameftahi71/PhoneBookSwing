package main.java.view;

import java.awt.EventQueue;
import java.awt.Font;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.io.IOException;

import javax.swing.JButton;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.JPasswordField;
import javax.swing.JTextField;

import org.quartz.xml.ValidationException;

import main.java.controller.PersonManager;
import main.java.controller.util.Connector;
import main.java.controller.util.Handler;
import main.java.controller.util.PopupBox;
import main.java.model.Person;

public class LoginFrame {

	private JFrame frameLogin;
	private JTextField tfUsername;
	private MenuFrame menuFrame;
	private JPasswordField tfPassword;
	private JTextField tfIp;
	private JPanel panelLogin;
	private JLabel lblUsername;
	private JLabel lblPassword;
	private JButton btnLogin;
	private JButton btnGuest;
	private JPanel panelConnect;
	private JButton btnConnect;
	private JLabel lblEnterServerIp;
	private boolean isEnabled = true;

	/**
	 * Launch the application.
	 */
	public static void main(String[] args) {
		EventQueue.invokeLater(new Runnable() {
			public void run() {
				try {
					LoginFrame window = new LoginFrame();
					window.frameLogin.setVisible(true);
				} catch (Exception e) {
					e.printStackTrace();
				}
			}
		});
	}

	/**
	 * Create the application.
	 */
	public LoginFrame() {
		initialize();
		PersonManager.setGuest(true);
		toggleDisableLoginPanel();
	}

	public void toggleDisableLoginPanel() {
		isEnabled = !isEnabled;
		tfUsername.setEnabled(isEnabled);
		tfPassword.setEnabled(isEnabled);
		lblUsername.setEnabled(isEnabled);
		lblPassword.setEnabled(isEnabled);
		btnLogin.setEnabled(isEnabled);
		btnGuest.setEnabled(isEnabled);
		panelConnect.setEnabled(!isEnabled);
		btnConnect.setEnabled(!isEnabled);
		tfIp.setEnabled(!isEnabled);
		lblEnterServerIp.setEnabled(!isEnabled);
	}

	/**
	 * Initialize the contents of the frame.
	 */
	private void initialize() {
		frameLogin = new JFrame();
		frameLogin.setResizable(false);
		frameLogin.setTitle("Login");
		frameLogin.setBounds(100, 100, 355, 325);
		frameLogin.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		frameLogin.getContentPane().setLayout(null);

		panelLogin = new JPanel();
		panelLogin.setBounds(10, 119, 327, 167);
		frameLogin.getContentPane().add(panelLogin);
		panelLogin.setLayout(null);

		tfUsername = new JTextField();
		tfUsername.setBounds(171, 11, 132, 30);
		panelLogin.add(tfUsername);
		tfUsername.setColumns(10);

		lblUsername = new JLabel("Username");
		lblUsername.setFont(new Font("Tahoma", Font.BOLD, 13));
		lblUsername.setBounds(10, 11, 88, 30);
		panelLogin.add(lblUsername);

		lblPassword = new JLabel("Password");
		lblPassword.setFont(new Font("Tahoma", Font.BOLD, 13));
		lblPassword.setBounds(10, 60, 88, 28);
		panelLogin.add(lblPassword);

		btnLogin = new JButton("Login");
		btnLogin.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent arg0) {
				try {
					Person person = new Person(tfUsername.getText(), Handler.getMD5Hash(tfPassword.getText()));
					int StatusCode = PersonManager.login(person);
					if (StatusCode == 200) {
						// create a new instance of the main window and hide this form
						PersonManager.setGuest(false);
						menuFrame = new MenuFrame();
						menuFrame.show();
						frameLogin.dispose();
					}
				} catch (RuntimeException | IOException e) {
					PopupBox.showError(e.getMessage(), "Error");
				}
			}
		});
		btnLogin.setFont(new Font("Tahoma", Font.BOLD, 14));
		btnLogin.setBounds(171, 113, 132, 30);
		panelLogin.add(btnLogin);

		btnGuest = new JButton("Enter as Guest");
		btnGuest.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent arg0) {
				menuFrame = new MenuFrame();
				menuFrame.show();
				frameLogin.dispose();
			}
		});
		btnGuest.setFont(new Font("Tahoma", Font.PLAIN, 13));
		btnGuest.setBounds(10, 113, 132, 30);
		panelLogin.add(btnGuest);

		tfPassword = new JPasswordField();
		tfPassword.setBounds(171, 60, 132, 30);
		panelLogin.add(tfPassword);

		panelConnect = new JPanel();
		panelConnect.setBounds(10, 11, 327, 97);
		frameLogin.getContentPane().add(panelConnect);
		panelConnect.setLayout(null);

		tfIp = new JTextField();
		tfIp.setFont(new Font("Tahoma", Font.PLAIN, 13));
		tfIp.setBounds(158, 11, 159, 30);
		panelConnect.add(tfIp);
		tfIp.setColumns(10);

		btnConnect = new JButton("Connect");
		btnConnect.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent arg0) {
				try {
					Connector.setIp(tfIp.getText());
					if (Connector.testConnection()) {
						PopupBox.showMessage("Seccessfully Connected to the server.", "Seccess");
						toggleDisableLoginPanel();
					}
				} catch (ValidationException e) {
					PopupBox.showError(e.getMessage(), "Error");
				}
			}
		});
		btnConnect.setFont(new Font("Tahoma", Font.PLAIN, 13));
		btnConnect.setBounds(158, 52, 159, 30);
		panelConnect.add(btnConnect);

		lblEnterServerIp = new JLabel("Enter Server IP");
		lblEnterServerIp.setFont(new Font("Tahoma", Font.PLAIN, 13));
		lblEnterServerIp.setBounds(10, 11, 132, 30);
		panelConnect.add(lblEnterServerIp);
	}
}
