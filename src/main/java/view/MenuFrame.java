package main.java.view;

import java.awt.BorderLayout;
import java.awt.EventQueue;
import java.awt.Font;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.io.IOException;
import javax.swing.JButton;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JPanel;
import main.java.controller.PersonManager;
import main.java.controller.util.PopupBox;

public class MenuFrame {

	private JFrame menuFrame;

	/**
	 * Launch the application.
	 */
	public void show() {
		EventQueue.invokeLater(new Runnable() {
			public void run() {
				try {
					MenuFrame window = new MenuFrame();
					window.menuFrame.setVisible(true);
				} catch (Exception e) {
					e.printStackTrace();
				}
			}
		});
	}

	/**
	 * Create the application.
	 */
	public MenuFrame() {
		initialize();
	}

	/**
	 * Initialize the contents of the menuFrame.
	 */
	private void initialize() {
		menuFrame = new JFrame();
		menuFrame.setTitle("Phone Book");
		menuFrame.setResizable(false);
		menuFrame.setBounds(100, 100, 543, 277);
		menuFrame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);

		JPanel panelMenu = new JPanel();
		menuFrame.getContentPane().add(panelMenu, BorderLayout.CENTER);
		panelMenu.setLayout(null);

		JButton btnAdd = new JButton("Add Contact");
		btnAdd.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent arg0) {
				new AddFrame().show();
			}
		});
		btnAdd.setFont(new Font("Tahoma", Font.PLAIN, 13));
		btnAdd.setBounds(105, 79, 110, 54);
		panelMenu.add(btnAdd);

		JButton btnShowContacts = new JButton("Contacts");
		btnShowContacts.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent arg0) {
				try {
					new ShowFrame().show();
				} catch (RuntimeException | IOException e) {
					PopupBox.showError(e.getMessage(), "Error");
				}

			}
		});
		btnShowContacts.setFont(new Font("Tahoma", Font.PLAIN, 13));
		btnShowContacts.setBounds(320, 79, 110, 54);
		panelMenu.add(btnShowContacts);

		JPanel panelHeader = new JPanel();
		menuFrame.getContentPane().add(panelHeader, BorderLayout.NORTH);

		JLabel lblWelcomeMessage = new JLabel("Welcome Dear " + PersonManager.getLoggedUsername() + "! ");
		panelHeader.add(lblWelcomeMessage);

		if (PersonManager.isGuest())
			btnAdd.setEnabled(false);
	}

}
