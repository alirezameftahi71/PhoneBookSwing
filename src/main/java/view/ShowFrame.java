package main.java.view;

import java.awt.EventQueue;
import java.io.IOException;
import java.util.ArrayList;
import java.util.List;
import javax.swing.JFrame;
import javax.swing.JScrollPane;
import javax.swing.JTable;
import main.java.controller.ContactManager;
import main.java.model.Contact;
import main.java.model.ContactTableModel;

public class ShowFrame {

	private JFrame frame;
	private JTable table;

	/**
	 * Launch the application.
	 */
	public void show() {
		EventQueue.invokeLater(new Runnable() {
			public void run() {
				try {
					ShowFrame window = new ShowFrame();
					window.frame.setVisible(true);
				} catch (Exception e) {
					e.printStackTrace();
				}
			}
		});
	}

	/**
	 * Create the application.
	 * @throws IOException 
	 * @throws RuntimeException 
	 */
	public ShowFrame() throws RuntimeException, IOException {
		initialize();
	}

	private List<Contact> loadData() throws RuntimeException, IOException {
		return (ArrayList<Contact>) ContactManager.getAll();

	}

	/**
	 * Initialize the contents of the frame.
	 * @throws IOException 
	 * @throws RuntimeException 
	 */
	private void initialize() throws RuntimeException, IOException {
		ContactTableModel model;
		frame = new JFrame("Contacts");
		frame.setDefaultCloseOperation(JFrame.DISPOSE_ON_CLOSE);
		model = new ContactTableModel(loadData());
		table = new JTable(model);
		frame.getContentPane().add(new JScrollPane(table));
		frame.pack();
	}

}
