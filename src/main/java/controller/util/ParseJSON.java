package main.java.controller.util;

import java.io.IOException;
import java.util.List;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.fasterxml.jackson.databind.type.TypeFactory;
import main.java.model.Contact;

public class ParseJSON {
	
	private static ObjectMapper mapper = null;

	public static String toJsonString(Object object) throws IOException {
		mapper = new ObjectMapper();
		return mapper.writeValueAsString(object);
	}

	public static List<Contact> toObject(String jsonString) throws IOException {
		mapper = new ObjectMapper();
		return mapper.readValue(jsonString,
				TypeFactory.defaultInstance().constructCollectionType(List.class, Contact.class));
	}

}
