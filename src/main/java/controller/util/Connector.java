package main.java.controller.util;

import java.io.IOException;
import java.util.regex.Pattern;
import org.apache.http.HttpResponse;
import org.apache.http.client.methods.HttpGet;
import org.apache.http.client.methods.HttpPost;
import org.apache.http.entity.StringEntity;
import org.apache.http.impl.client.CloseableHttpClient;
import org.apache.http.impl.client.HttpClientBuilder;
import org.quartz.xml.ValidationException;

public class Connector {

	private static String ip = null;

	public static String getIp() {
		return ip;
	}

	public static void setIp(String ip) {
		Connector.ip = ip.trim();
	}

	public static boolean testConnection() throws ValidationException {
		String url = "http://" + Connector.getIp() + ":8080/JeePhoneBook/api/test/";
		CloseableHttpClient httpClient = null;
		HttpGet getRequest = null;
		HttpResponse response = null;
		if (Connector.getIp().equals(""))
			throw new ValidationException("The IP not set.");
		boolean isIpValid = Pattern.compile("\\b\\d{1,3}\\.\\d{1,3}\\.\\d{1,3}\\.\\d{1,3}\\b")
				.matcher(Connector.getIp()).find();
		boolean isLocalhost = Connector.getIp().toLowerCase().equals("localhost") ? true : false;
		if (!isIpValid)
			if (!isLocalhost)
				throw new ValidationException("The IP is not Valid.");
		try {
			httpClient = HttpClientBuilder.create().build();
			getRequest = new HttpGet(url);
			getRequest.addHeader("accept", "application/json");
			response = httpClient.execute(getRequest);
			httpClient.close();
		} catch (IOException e) {
			PopupBox.showError("Cannot Connect to the server.", "Error");
		}
		if (response != null && response.getStatusLine().getStatusCode() == 200)
			return true;
		return false;
	}

	public static String httpRequestGET(String url) throws IOException {
		CloseableHttpClient httpClient = HttpClientBuilder.create().build();
		HttpGet getRequest = new HttpGet(url);
		getRequest.addHeader("accept", "application/json");
		HttpResponse response = httpClient.execute(getRequest);
		if (response.getStatusLine().getStatusCode() != 200)
			throw new RuntimeException("Failed : HTTP error code : " + response.getStatusLine().getStatusCode() + "\n"
					+ Handler.getContent(response).toString());
		String resultContent = Handler.getContent(response);
		httpClient.close();
		return resultContent;
	}

	public static int httpRequestPOST(String url, String jsonString) throws IOException {
		CloseableHttpClient httpClient = HttpClientBuilder.create().build();
		HttpPost postRequest = new HttpPost(url);
		StringEntity input = new StringEntity(jsonString);
		input.setContentType("application/json");
		postRequest.setEntity(input);
		HttpResponse response = httpClient.execute(postRequest);
		if (response.getStatusLine().getStatusCode() != 200)
			throw new RuntimeException("Failed : HTTP error code : " + response.getStatusLine().getStatusCode() + "\n"
					+ Handler.getContent(response).toString());
		int resultCode = Handler.getStatusCode(response);
		httpClient.close();
		return resultCode;
	}

}