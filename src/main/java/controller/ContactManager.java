package main.java.controller;

import java.io.IOException;
import java.util.List;

import main.java.controller.util.Connector;
import main.java.controller.util.ParseJSON;
import main.java.model.Contact;

public class ContactManager {

	public static int add(Contact contact) throws RuntimeException, IOException {
		String jsonString = ParseJSON.toJsonString(contact);
		String url = "http://" + Connector.getIp() + ":8080/JeePhoneBook/api/contact/items/";
		return Connector.httpRequestPOST(url, jsonString);
	}

	public static List<Contact> getAll() throws RuntimeException, IOException {
		String url = "http://" + Connector.getIp() + ":8080/JeePhoneBook/api/contact/items/";
		return ParseJSON.toObject(Connector.httpRequestGET(url));
	}

}
