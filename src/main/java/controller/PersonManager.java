package main.java.controller;

import java.io.IOException;

import main.java.controller.util.Connector;
import main.java.controller.util.ParseJSON;
import main.java.model.Person;

public class PersonManager {

	private static boolean isGuest = true;
	private static Person person;

	public static String getLoggedUsername() {
		if (isGuest)
			return "Guest";
		return PersonManager.person.getUsername();
	}

	public static boolean isGuest() {
		return isGuest;
	}

	public static void setGuest(boolean isGuest) {
		PersonManager.isGuest = isGuest;
	}

	public static int login(Person p) throws IOException, RuntimeException {
		person = p;
		// converting the object to json string
		String jsonString = ParseJSON.toJsonString(person);
		String url = "http://" + Connector.getIp() + ":8080/JeePhoneBook/api/person/items/login/";
		// sending the json to the service
		return Connector.httpRequestPOST(url, jsonString);
	}

}
